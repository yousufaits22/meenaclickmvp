package com.example.meenaclickmvp.session

import android.content.Context
import android.content.SharedPreferences
import android.widget.Toast
import com.example.meenaclickmvp.model.CatalogProductsItem
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class SharedPreferencesImpl(val context: Context): Session {

    val TAG  = "SharedPreferencesImpl"

    lateinit var sharedPreferences: SharedPreferences
    lateinit var gson: Gson


    override fun addToCart(productsItem: CatalogProductsItem) {

        if (isProductExist(productsItem)){
            return
        }
        sharedPreferences = context.getSharedPreferences(TAG, Context.MODE_PRIVATE)
        gson = Gson()
        val  cartProductList = getCartAllProduct()
        cartProductList.add(productsItem)
        productsItem.stockQty

        val cartProduct = gson.toJson(cartProductList)
        print(cartProduct)

        sharedPreferences.edit().putString("cart_product", cartProduct).apply()
    }

    override fun isProductExist(productsItem: CatalogProductsItem): Boolean {
        sharedPreferences=context.getSharedPreferences(TAG,Context.MODE_PRIVATE)
        val json=sharedPreferences.getString("cart_product",null)
        var isfound = false
        if (json!=null){
            val searchItem=getCartAllProduct()
            for (i in searchItem){
                if (i?.productId == productsItem.productId){
                    Toast.makeText(context,"This Products alreay in Cart", Toast.LENGTH_SHORT).show()
                    isfound=true
                }
            }
        }
        return isfound
    }

    override fun getCartAllProduct(): ArrayList<CatalogProductsItem?> {
        sharedPreferences=context.getSharedPreferences(TAG,Context.MODE_PRIVATE)

        val listType =
            object : TypeToken<List<CatalogProductsItem?>?>() {}.type
        var productsItemList:ArrayList<CatalogProductsItem?> = ArrayList()
        val json=sharedPreferences.getString("cart_product",null)

        if (json !=null){
            gson=Gson()
            productsItemList = gson.fromJson(json,listType)
        }
        return productsItemList
    }

    override fun removeProduct(productsItem: CatalogProductsItem) {
        sharedPreferences = context.getSharedPreferences(TAG, Context.MODE_PRIVATE)
        val stringCheck = sharedPreferences.getString("cart_product", null)
        if (stringCheck != null) {
            val searchItem = getCartAllProduct()
            val typelist = object : TypeToken<ArrayList<CatalogProductsItem?>?>() {}.type
            gson = Gson()
            searchItem.remove(productsItem)
            var json = gson.toJson(searchItem)
            sharedPreferences.edit().putString("cart_product", json).apply()
        }
    }

    override fun updateCartQtyWithProduct(qty: Int, sItem: CatalogProductsItem) {
        sharedPreferences = context.getSharedPreferences(TAG, Context.MODE_PRIVATE)
        val cartPresentItem = getCartAllProduct()

        for ((index,value) in cartPresentItem.withIndex()){
            if (value?.productId == sItem.productId){
                cartPresentItem.get(index)?.cartQty = qty
            }
        }

        gson = Gson()
        var jsonData = gson.toJson(cartPresentItem)
        sharedPreferences.edit().putString("cart_product", jsonData).apply()
    }
}