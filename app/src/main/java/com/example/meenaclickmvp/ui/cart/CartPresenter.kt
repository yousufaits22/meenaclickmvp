package com.example.meenaclickmvp.ui.cart

import android.util.Log
import com.example.meenaclickmvp.model.CatalogProductsItem
import com.example.meenaclickmvp.session.SharedPreferencesImpl

class CartPresenter(val view: CartContract.View, private val session: SharedPreferencesImpl): CartContract.Presenter {

    override fun getCartItem() {
        val cartItemList = session.getCartAllProduct()
        view.setDataIntoAdapter(cartItemList)

        val value = session.getCartAllProduct().size
        view.getCartValue(value)
    }

    override fun updateCartQty(qty: Int, item: CatalogProductsItem) {
        session.updateCartQtyWithProduct(qty, item)
    }

    override fun calculateCartPrice() {
        val cartAllProduct= session.getCartAllProduct()
        var totalPrice = 0
        for (item in cartAllProduct){
            totalPrice += item?.price!! * item.cartQty!!
        }
        Log.e("call", "$totalPrice")
        view.setCartPrice(totalPrice)
    }
}