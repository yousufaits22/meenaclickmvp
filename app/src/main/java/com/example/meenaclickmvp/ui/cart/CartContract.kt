package com.example.meenaclickmvp.ui.cart

import com.example.meenaclickmvp.model.CatalogProductsItem

class CartContract {
    interface View{
        fun setDataIntoAdapter(list: ArrayList<CatalogProductsItem?>)
        fun getCartValue(value: Int)
        fun setCartPrice(price : Int)
    }
    interface Presenter{
        fun getCartItem()
        fun updateCartQty( qty: Int, item: CatalogProductsItem)
        fun calculateCartPrice()
    }
}